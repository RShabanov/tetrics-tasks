from collections import Counter
from concurrent.futures import ThreadPoolExecutor, as_completed
from pathlib import Path
import requests
from bs4 import BeautifulSoup


def _animals_number_by_letter(soup: BeautifulSoup, letter: str) -> Counter:
    animal_counter = Counter()
    pages = soup.find(id="mw-pages")
    
    if pages is not None:
        groups = pages.find_all("div", {"class": "mw-category-group"})
        
        if len(groups) > 0:
            
            for group in groups:
                if group.h3 is not None and group.h3.text.lower() == letter.lower():
                    animal_list = group.find_all("li")
                    animal_counter[group.h3.text] += len(animal_list)
                    
    return animal_counter
    
    
def _next_animal_route(soup: BeautifulSoup) -> str | None:
    pages = soup.find(id="mw-pages")
    
    if pages is not None:
        route = pages.find("a", string="Следующая страница", href=True)
        
        if route is not None:
            return route["href"]
        
    return None


def animal_list_by_letter(letter: str, url: str, parser: str = "lxml") -> tuple[str, int]:
    cnt = 0
    
    try:
        while True:
            response = requests.get(url)
        
            if response.ok:
                soup = BeautifulSoup(response.content, parser)
                animal_counter = _animals_number_by_letter(soup, letter)
                
                if len(animal_counter) > 0:
                    cnt += animal_counter[letter]
                    
                    if len(animal_counter) != 1 or animal_counter[letter] == 0:
                        break
                    
                else: break
                
                route = _next_animal_route(soup)
                if route is not None:
                    url = f"{BASE_URL}{route}"
                else: break
            
    except requests.exceptions.RequestException as e:
        print(e)
        
    return (letter, cnt)


def _get_all_letters(soup: BeautifulSoup) -> dict[str, str]:
    table = soup.find("table", {"class": "plainlinks"})
    
    links = table.find_all("a")
    links = dict(map(lambda tag: (tag.text, tag["href"]), links))
    letters = {letter: link for letter, link in links.items() if letter.isalpha() and len(letter) == 1}
    
    return letters


def count_animals(url: str) -> Counter:
    animals = Counter()
    
    try:
        response = requests.get(url)
        
        if response.ok:
            soup = BeautifulSoup(response.content, "lxml")
            letters_dict = _get_all_letters(soup=soup)
            
            with ThreadPoolExecutor(max_workers=len(letters_dict)) as executor:
                futures = [executor.submit(animal_list_by_letter, letter, url) for letter, url in letters_dict.items()]
                
                for future in as_completed(futures):
                    result = future.result()
                    
                    if result is not None:
                        letter, cnt = result
                        animals[letter] += cnt
    except requests.exceptions.RequestException as e:
        print(e)
    
    return animals


if __name__ == "__main__":
    BASE_URL = "https://ru.wikipedia.org"
    url = "https://inlnk.ru/jElywR"
    
    
    animals = count_animals(url=url)
    list_str = []
    
    for letter in sorted(animals.keys()):
        list_str.append(f"{letter}: {animals[letter]}")
    
    filename = Path(__file__).parent / "animals.txt"
    with open(filename, 'w') as fp:
        fp.write('\n'.join(list_str))
    