def index_of_one(array: str | list):
    try:
        if isinstance(array, list):
           array = ''.join(map(str, array))
            
        return array.rfind('1') # since list.index raises ValueError
    except Exception as e:
        print(e)
        

def iou(
    x1: float, y1: float,
    x2: float, y2: float,
    x3: float, y3: float,
    x4: float, y4: float
) -> float:
    """Returns intersection area"""
    
    xi1 = max(x1, x3)
    yi1 = max(y1, y3)
    xi2 = min(x2, x4)
    yi2 = min(y2, y4)
    
    if xi1 >= xi2 or yi1 >= yi2:
        return 0
    
    return (xi2 - xi1) * (yi2 - yi1)


if __name__ == "__main__":
    
    assert index_of_one("111111111110000000000000000") == 10
    assert index_of_one("0000000000000000") == -1
    
    bbox1 = [0, 0, 10, 10]
    bbox2 = [0, 1, 10, 10]
    bbox3 = [20, 20, 30, 30]
    bbox4 = [5, 5, 15, 15]
    bbox5 = [1, 1, 2, 2]
    bbox6 = [3, 3, 4, 4]
    
    assert iou(*bbox1, *bbox1) == 100
    assert iou(*bbox1, *bbox2) == 90
    assert iou(*bbox1, *bbox3) == 0
    assert iou(*bbox1, *bbox4) == 25
    assert iou(*bbox5, *bbox6) == 0   
    
