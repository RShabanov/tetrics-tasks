def _appearance():
    ...
    
    
def _intersect_timestamp(timestamp_1: list, timestamp_2: list) -> list:
    return [
        max(timestamp_1[0], timestamp_2[0]),
        min(timestamp_1[1], timestamp_2[1]),
    ]
    
    
def _is_intersection(interval_1: list, interval_2: list) -> bool:
    return interval_1[0] <= interval_2[0] <= interval_1[1] or \
        interval_1[0] <= interval_2[1] <= interval_1[1]


def _join_timestamps(timestamp_1: list, timestamp_2: list) -> list:
    return [
        min(timestamp_1[0], timestamp_2[0]),
        max(timestamp_1[1], timestamp_2[1]),
    ]
    

def _join_intervals(intervals: list) -> list:
    new_interval = []
    
    idx = 0
    interval = interval = intervals[idx:idx + 2]
    is_intersection = False
    
    for idx in range(2, len(intervals), 2):           
        o_interval = intervals[idx: idx + 2]
        is_intersection = _is_intersection(interval, o_interval)
        
        if is_intersection:
            interval = _join_timestamps(interval, o_interval)
        else:
            new_interval.extend(interval)
            interval = o_interval
            
    if not is_intersection:
        new_interval.extend(interval)
    
    return new_interval


def appearance(intervals):
    lesson = intervals["lesson"]
    pupil = _join_intervals(intervals["pupil"])
    tutor = _join_intervals(intervals["tutor"])
    
    total_time = 0
    
    for t_idx in range(0, len(tutor), 2):
        interval = tutor[t_idx:t_idx + 2]
        
        if _is_intersection(lesson, interval):
            interval = _intersect_timestamp(lesson, interval)
            
            for p_idx in range(0, len(pupil), 2):
                p_interval = pupil[p_idx:p_idx + 2]
                
                if _is_intersection(lesson, p_interval):
                    p_interval = _intersect_timestamp(lesson, p_interval)
                
                if _is_intersection(interval, p_interval):
                    total_interval = _intersect_timestamp(p_interval, interval)
                    total_time += total_interval[-1] - total_interval[0]
                    
    return total_time
    

tests = [
    {
        'data': {
            'lesson': [1594663200, 1594666800],
            'pupil': [
                1594663340, 1594663389, 
                1594663390, 1594663395, 
                1594663396, 1594666472
            ],
            'tutor': [
                1594663290, 1594663430, 
                1594663443, 1594666473
            ]
        },
        'answer': 3117
    },
    {
        'data': {
            'lesson': [1594702800, 1594706400],
            'pupil': [
                1594702789, 1594704500, 
                1594702807, 1594704542, 
                1594704512, 1594704513, 
                1594704564, 1594705150, 
                1594704581, 1594704582, 
                1594704734, 1594705009, 
                1594705095, 1594705096, 
                1594705106, 1594706480, 
                1594705158, 1594705773, 
                1594705849, 1594706480, 
                1594706500, 1594706875, 
                1594706502, 1594706503, 
                1594706524, 1594706524, 
                1594706579, 1594706641
            ],
            'tutor': [
                1594700035, 1594700364, 
                1594702749, 1594705148, 
                1594705149, 1594706463
            ]
        },
        'answer': 3577
    },
    {
        'data': {
            'lesson': [1594692000, 1594695600],
            'pupil': [1594692033, 1594696347],
            'tutor': [
                1594692017, 1594692066, 
                1594692068, 1594696341
            ]
        },
        'answer': 3565
    },
]


if __name__ == '__main__':
    for i, test in enumerate(tests):
        test_answer = appearance(test['data'])
        assert test_answer == test['answer'], f'Error on test case {i}, got {test_answer}, expected {test["answer"]}'
        